defmodule ServiceA.Endpoint do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  # # just match anything and end a 200
  # match _ do
  #   send_resp(conn, 200, "A RESPONSE")
  # end

  get "/hello" do
    send_resp(conn, 200, "world")
  end

  get _ do
    send_resp(conn, 400, "not found")
  end
end
