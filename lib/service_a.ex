defmodule ServiceA do
  @moduledoc """
  Documentation for ServiceA.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ServiceA.hello()
      :world

  """
  def hello do
    :world
  end
end
